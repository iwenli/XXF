﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.Common
{
    public static  class Utils
    {
        /// <summary>
        /// 获取集合的指定分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">页码(从1开始)</param>
        /// <param name="recordCount">返回总项目数</param>
        /// <param name="pageCount">返回总页数</param>
        /// <returns></returns>
        public static IList<T> PageList<T>(ICollection<T> collection, int pageSize, int pageIndex, ref int recordCount)
        {
            if (collection == null)
            {
                recordCount = 0;
                pageIndex = 0;
                return new List<T>(0);
            }

            recordCount = collection.Count;
            if (recordCount < 1)
            {
                pageIndex = 0;
                return new List<T>(0);
            }


            if (pageSize < 1)
                pageSize = 1;
            if (pageSize > recordCount)
                pageSize = recordCount;


            if (pageIndex < 1)
                pageIndex = 1;

            if (pageIndex > 1)
            {
                return collection.Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
            }
            else
            {
                return collection.Take(pageSize).ToList();
            }
        }
        /// <summary>
        /// 获取集合的指定分页数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">页码(从1开始)</param>
        /// <param name="recordCount">返回总项目数</param>
        /// <param name="pageCount">返回总页数</param>
        /// <returns></returns>
        public static IList<T> PageList<T>(IEnumerable<T> enumerable, int pageSize, int pageIndex, ref int recordCount)
        {
            if (enumerable == null)
            {
                recordCount = 0;
                pageIndex = 0;
                return new List<T>(0);
            }

            recordCount = enumerable.Count();
            if (recordCount < 1)
            {
                pageIndex = 0;
                return new List<T>(0);
            }


            if (pageSize < 1)
                pageSize = 1;
            if (pageSize > recordCount)
                pageSize = recordCount;


            if (pageIndex < 1)
                pageIndex = 1;

            if (pageIndex > 1)
            {
                return enumerable.Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
            }
            else
            {
                return enumerable.Take(pageSize).ToList();
            }
        }
    }
}
