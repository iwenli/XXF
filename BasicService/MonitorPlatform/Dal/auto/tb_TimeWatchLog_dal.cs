using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BasicService.MonitorPlatform.Model;

namespace XXF.BasicService.MonitorPlatform.Dal
{

	public partial class tb_TimeWatchLog_dal
    {
        public virtual bool Add(DbConn PubConn, tb_TimeWatchLog_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>()
                {
					
					//类型：0 默认，1 sql，2 功能
					new ProcedureParameter("@Type",    model.Type),
					//创建时间
					new ProcedureParameter("@CreateTime",    model.CreateTime),
					//耗时
					new ProcedureParameter("@Time",    model.Time),
					//项目名称
					new ProcedureParameter("@ProjectName",    model.ProjectName),
					//请求url
					new ProcedureParameter("@Url",    model.Url),
					//消息日志
					new ProcedureParameter("@Msg",    model.Msg),
					//其他标记备注
					new ProcedureParameter("@Tag",    model.Tag)   
                };
            int rev = PubConn.ExecuteSql(@"insert into tb_TimeWatchLog(Type,CreateTime,Time,ProjectName,Url,Msg,Tag)
										   values(@Type,@CreateTime,@Time,@ProjectName,@Url,@Msg,@Tag)", Par);
            return rev == 1;

        }

        public virtual bool Edit(DbConn PubConn, tb_TimeWatchLog_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
                    
					//类型：0 默认，1 sql，2 功能
					new ProcedureParameter("@Type",    model.Type),
					//创建时间
					new ProcedureParameter("@CreateTime",    model.CreateTime),
					//耗时
					new ProcedureParameter("@Time",    model.Time),
					//项目名称
					new ProcedureParameter("@ProjectName",    model.ProjectName),
					//请求url
					new ProcedureParameter("@Url",    model.Url),
					//消息日志
					new ProcedureParameter("@Msg",    model.Msg),
					//其他标记备注
					new ProcedureParameter("@Tag",    model.Tag)
            };
			Par.Add(new ProcedureParameter("@id",  model.ID));

            int rev = PubConn.ExecuteSql("update tb_TimeWatchLog set Type=@Type,CreateTime=@CreateTime,Time=@Time,ProjectName=@ProjectName,Url=@Url,Msg=@Msg,Tag=@Tag where id=@id", Par);
            return rev == 1;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id",  id));

            string Sql = "delete from tb_TimeWatchLog where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual tb_TimeWatchLog_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", ProcParType.Int32, 4, id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_TimeWatchLog s where s.id=@id");
            int rev = PubConn.ExecuteSql(stringSql.ToString(), Par);
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0)
            {
				return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

		public virtual tb_TimeWatchLog_model CreateModel(DataRow dr)
        {
            return new tb_TimeWatchLog_model
            {
				
				//
				ID = dr["ID"].Toint(),
				//类型：0 默认，1 sql，2 功能
				Type = dr["Type"].ToByte(),
				//创建时间
				CreateTime = dr["CreateTime"].ToDateTime(),
				//耗时
				Time = dr["Time"].Todouble(),
				//项目名称
				ProjectName = dr["ProjectName"].Tostring(),
				//请求url
				Url = dr["Url"].Tostring(),
				//消息日志
				Msg = dr["Msg"].Tostring(),
				//其他标记备注
				Tag = dr["Tag"].Tostring()
            };
        }
    }
}