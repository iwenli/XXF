﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace XXF.BasicService.NotifyCenter
{
    /// <summary>
    /// 消息中心配置
    /// </summary>
    public static class NCConfig
    {
        private static string apiUrl;
        /// <summary>
        /// 访问的API地址
        /// </summary>
        public static string ApiUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(apiUrl))
                {
                    return ConfigurationManager.AppSettings["NCApiUrl"];
                    //读取配置中心
                    //return XXF.BaseService.ConfigManager.ConfigManagerHelper.Get<string>("NCApiUrl");//获取配置值
                }
                else
                {
                    return apiUrl;
                }
            }
            set
            {
                apiUrl = value;
            }
        }
    }
}
