﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.IO;
using Newtonsoft.Json;
using System.Xml.Serialization;
using XXF.BasicService.ConfigCenter.Cached;

namespace XXF.BasicService.ConfigCenter.Xml
{
    public class XmlHandle
    {
        public static readonly XmlHandle Instance = new XmlHandle();
        private static string name = "ConfigCenter.config";
        private string path = System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + "Config\\";

        public void CreateXml(ConfigsModel Configs)
        {
            XmlDocument doc = new XmlDocument();
            if (File.Exists(path + "/" + name))
                File.Delete(path + "/" + name);
            XXF.Serialization.XmlProvider<ConfigsModel> provider = new Serialization.XmlProvider<ConfigsModel>();
            object obj = provider.Serializer(Configs);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            doc.LoadXml(obj.ToString());
            doc.Save(path + "/" + name);
        }
        /// <summary>得到单个值</summary>
        /// <param name="csmc"></param>
        /// <param name="xmdh"></param>
        /// <returns></returns>
        public string GetConfigXml(string csmc, string xmdh)
        {
            ConfigsModel configs = GetConfigModel();
            if (configs == null)
                return null;
            string csz = null;
            Config config = configs.Configs.FirstOrDefault(o => o.csmc == csmc && o.xmdh == xmdh);
            if (config == null)
                config = configs.Configs.FirstOrDefault(o => o.csmc == csmc && o.xmdh == "Config");
            if (config == null)
                return csz;
            csz = config.csz;
            return csz;
        }
        /// <summary>得到Xml字符串</summary>
        /// <returns></returns>
        public ConfigsModel GetConfigModel()
        {
            XmlDocument doc = new XmlDocument();
            if (!File.Exists(path + "/" + name))
                return null;
            doc.Load(path + "/" + name);
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = new XmlTextWriter(sw);
            doc.WriteTo(tw);
            XXF.Serialization.XmlProvider<ConfigsModel> provider = new XXF.Serialization.XmlProvider<ConfigsModel>();
            ConfigsModel configs = provider.Deserialize(sw.ToString());
            return configs;
        }
        /// <summary>得到list</summary>
        /// <param name="xmdh"></param>
        /// <returns></returns>
        public List<Config> GetConfigXmlList(string xmdh)
        {
            ConfigsModel configs = GetConfigModel();
            if (configs == null)
                return null;
            List<Config> config = configs.Configs.Where(o => o.xmdh == xmdh).ToList();
            if (config == null)
                return null;
            return config;
        }
    }
}
