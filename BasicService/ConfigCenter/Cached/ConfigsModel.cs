﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BasicService.ConfigCenter.Cached
{
    public class ConfigsModel
    {
        public List<Config> Configs { get; set; }
        public string Update { get; set; }
        public ConfigsModel Clone()
        {
            return new ConfigsModel()
            {
                Configs = Config.CloneList(this.Configs) as List<Config>,
                Update = this.Update as string
            };
        }

        public static List<ConfigsModel> CloneList(List<ConfigsModel> mother)
        {
            List<ConfigsModel> son = new List<ConfigsModel>();
            foreach (var item in mother)
            {
                son.Add(item.Clone() as ConfigsModel);
            }
            return son;
        }
    }

    public class Config
    {
        public string csmc { get; set; }
        public string csz { get; set; }
        public string xmdh { get; set; }

        public Config Clone()
        {
            return new Config()
            {
                csmc = this.csmc,
                csz = this.csz,
                xmdh = this.xmdh
            };
        }

        public static List<Config> CloneList(List<Config> mother)
        {
            if (mother == null)
                return new List<Config>();
            List<Config> son = new List<Config>();
            foreach (var item in mother)
            {
                son.Add(item.Clone() as Config);
            }
            return son;
        }
    }
}
